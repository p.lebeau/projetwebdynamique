<?php

//On fais appel au fichier permettant la connexion à la bdd
session_start(); 
?>

<html lang="fr">

   <head>
    
    <meta name="viewport" content="width=device-width" /> <!-- Afin de mettre en place un menu responsive, il faut obligatoirement ajouter cette balise à notre HTML permettant de communiquer à notre CSS la taille de l'écran de l'utilisateur --> 
    <link rel="stylesheet" type="text/css" href="style.css" />
    <title> Projet Web dynamique </title>

  </head>
    

  <body>

     <header>
        <div class="centrer">  <img class="logo" src="css/logo.gif" alt="logo"/> </div>
     </header>

  </body>
    
</html>

<?php

//On fais appel au fichier permettant la connexion à la bdd   
require('connexionbdd.php');
     

if (!$con)
{
    echo "Connexion à la base de données impossible";
}

else {
        $login = htmlentities($_POST['login'], ENT_QUOTES, "ISO-8859-1"); // le htmlentities() passera les guillemets en entités HTML, ce qui empêchera les injections SQL
        $_SESSION['login']=$login;
        $mdp = htmlentities($_POST['mdp'], ENT_QUOTES, "ISO-8859-1");
        $mdpcryp=md5($mdp);
    
//REQUETE POUR ETUDIANT
    $RequeteEtud = mysqli_query($con,"SELECT * FROM ETUDIANT WHERE id_e = '".$login."' AND password_e = '".$mdpcryp."'");
    
//REQUETE POUR PROFESSEUR
    $RequeteProf = mysqli_query($con,"SELECT * FROM PROFESSEUR WHERE id_p = '".$login."' AND password_p = '".$mdpcryp."'");
    
//REQUETE POUR ADMINISTRATION
    $RequeteAdm = mysqli_query($con,"SELECT * FROM ADMINISTRATION WHERE id_a = '".$login."' AND password_a = '".$mdpcryp."'");

//CONDITION SI ETUDIANT
    if(mysqli_num_rows($RequeteEtud) == 1) {
        //header("Location: eleve.php");
        $donnee=mysqli_fetch_array($RequeteEtud);
        
                $RequeteAbs = mysqli_query($con,"SELECT (SUM(`duree_abs`)/540*100) AS tx, `id_e` FROM ABSENCE where id_e = '".$login."'");
    
              if(mysqli_num_rows($RequeteAbs) == 1) {
                $don=mysqli_fetch_array($RequeteAbs);
                echo '<div id=fenetre> <center> <B> ESPACE ELEVE </B> </center> <br> Bonjour '.$donnee['prenom_e'].'.Votre taux d\'absence est de ' .$don['tx']. '% </div>';
              }
        
        else {
            header("Location: index.php");
        }
            
            
} 

//CONDITION SI PROFESSEUR
       elseif(mysqli_num_rows($RequeteProf)+1 == 2) {
           $donnee=mysqli_fetch_array($RequeteProf);
           $user=2;
           $_SESSION['user']=$user;
           
           echo '<div id=fenetre> <center> <B> PROFESSEUR </B> </center> <br> Bonjour '.$donnee['prenom_p'].',<br>Pour accèder vers votre espace, <a href="prof.php">cliquez-ici </a></div>' ;
        
}

//CONDITION SI ADMINISTRATION
        elseif(mysqli_num_rows($RequeteAdm)+2 == 3) {
            $donnee=mysqli_fetch_array($RequeteAdm);
            $user=3;
            $_SESSION['user']=$user;

            echo '<div id=fenetre> <center> <B> ADMINISTRATION </B> </center> <br> Bonjour '.$donnee['prenom_a'].',<br>Pour accèder vers votre espace, <a href="prof.php">cliquez-ici </a></div>' ;

            
            
}
    
else {
    header("location: index.php");
}
    
}
      
?>


