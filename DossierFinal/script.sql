-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:8889
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `m2105`
--

-- --------------------------------------------------------

--
-- Structure de la table `ABSENCE`
--

CREATE TABLE `ABSENCE` (
  `num_abs` int(11) NOT NULL,
  `duree_abs` int(255) DEFAULT NULL,
  `date_abs` date DEFAULT NULL,
  `id_e` varchar(25) NOT NULL,
  `num_m` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ABSENCE`
--

INSERT INTO `ABSENCE` (`num_abs`, `duree_abs`, `date_abs`, `id_e`, `num_m`) VALUES
(60, 1, '2017-06-10', '35000025', 'M1101'),
(61, 1, '2017-06-10', '35000028', 'M1101'),
(62, 1, '2017-06-14', '36000007', 'M2202'),
(63, 1, '2017-06-16', '36000008', 'M2101'),
(64, 3, '2017-06-16', '36000006', 'M2202'),
(65, 4, '2017-06-16', '35000024', 'M2202'),
(68, 1, '2017-06-16', '34000006', 'M2102'),
(69, 4, '2017-06-16', '36000005', 'M2201'),
(70, 4, '2017-06-16', '36000005', 'M2201'),
(71, 4, '2017-06-16', '36000005', 'M2201'),
(72, 4, '2017-06-16', '36000005', 'M2201'),
(73, 4, '2017-06-16', '36000005', 'M2201'),
(74, 4, '2017-06-16', '36000005', 'M2201'),
(75, 4, '2017-06-16', '36000005', 'M2201'),
(76, 4, '2017-06-16', '36000005', 'M2201'),
(77, 4, '2017-06-16', '36000005', 'M2201'),
(78, 4, '2017-06-17', '36000005', 'M2201');

-- --------------------------------------------------------

--
-- Structure de la table `ADMINISTRATION`
--

CREATE TABLE `ADMINISTRATION` (
  `id_a` varchar(25) NOT NULL,
  `password_a` varchar(255) DEFAULT NULL,
  `nom_a` varchar(25) DEFAULT NULL,
  `prenom_a` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ADMINISTRATION`
--

INSERT INTO `ADMINISTRATION` (`id_a`, `password_a`, `nom_a`, `prenom_a`) VALUES
('4567654345687', 'f75b775ff1286cab98964203e9433dd8', 'MOREL', 'Johanne');

-- --------------------------------------------------------

--
-- Structure de la table `ETUDIANT`
--

CREATE TABLE `ETUDIANT` (
  `id_e` varchar(25) NOT NULL,
  `password_e` varchar(255) DEFAULT NULL,
  `nom_e` varchar(25) DEFAULT NULL,
  `prenom_e` varchar(25) DEFAULT NULL,
  `datenaiss_e` varchar(25) DEFAULT NULL,
  `annee_e` varchar(25) DEFAULT NULL,
  `mail_e` varchar(25) DEFAULT NULL,
  `tp_e` int(11) DEFAULT NULL,
  `td_e` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `ETUDIANT`
--

INSERT INTO `ETUDIANT` (`id_e`, `password_e`, `nom_e`, `prenom_e`, `datenaiss_e`, `annee_e`, `mail_e`, `tp_e`, `td_e`) VALUES
('34000001', '5a23421037b09d01a029c4728f09fc4f', 'MAISON', 'Antoine', '01/01/1996', '3', 'emailetuLP1@example.com', 1, 1),
('34000002', '3c9678c4b64fd2bd415a261610650dde', 'ALFRED', 'Nathan', '01/01/1996', '3', 'emailetuLP2@example.com', 1, 1),
('34000003', '832beddbb97c2b841232604d83e7b296', 'IGOUF', 'Filoor', '03/01/1996', '3', 'emailetuLP3@example.com', 1, 1),
('34000004', '84c63d596ce50d2cb2aac50e3532599d', 'MALBROUCK', 'Lurgog', '05/01/1996', '3', 'emailetuLP4@example.com', 1, 1),
('34000005', '3e6ae659a89b19b23321253b87f0811a', 'FOLLET', 'Jarbowyn', '07/01/1996', '3', 'emailetuLP5@example.com', 1, 1),
('34000006', '9e494e458a1c621b80334b32e6cf57a6', 'ROBERT', 'Bolog', '09/01/1996', '3', 'emailetuLP6@example.com', 1, 1),
('34000007', '708728db7bef8c32d1a6de8d3eb7724a', 'VIDOT', 'Tirvinn', '11/01/1996', '3', 'emailetuLP7@example.com', 1, 1),
('34000008', '982c158eb979e04709fb565dfb7b7f36', 'COMBO', 'Nymonemand', '15/01/1996', '3', 'emailetuLP8@example.com', 1, 1),
('34000009', '64a6d276f93ded37a89b3b2c9db8b798', 'TAHI', 'Vilebald', '19/01/1996', '3', 'emailetuLP9@example.com', 1, 1),
('34000010', 'cdb8028f40a5313f0df179a1414855ac', 'AUDIGER', 'Luzrod', '21/01/1996', '3', 'emailetuLP10@example.com', 1, 1),
('34000011', '1daef2d830d87a5a98ec2282449ea046', 'PAYET', 'Balkhagal', '23/01/1996', '3', 'emailetu1LP1@example.com', 1, 1),
('34000012', '27974733f5b7f65c883c93785fa3f88d', 'LEBO', 'France', '26/01/1996', '3', 'emailetuLP12@example.com', 2, 1),
('34000013', '2ee372758e4c11f3d961f5fbb7525e49', 'PANECHOU', 'Filip', '27/01/1996', '3', 'emailetuLP13@example.com', 2, 1),
('34000014', '6d29b78a516f9b3b25416d84bfbdbcac', 'GRONDIN', 'Thomas', '28/01/1996', '3', 'emailetuLP14@example.com', 2, 1),
('34000015', 'd9a78c5e0374fd2a15ff85618f4561cd', 'PAUSET', 'Odreneard', '29/01/1996', '3', 'emailetuLP15@example.com', 2, 1),
('34000016', 'ba03edbc7e7206c7342b40ce61b49d20', 'POSE', 'Linnavia', '30/01/1996', '3', 'emailetuLP16@example.com', 2, 1),
('34000017', 'f372fd24ab2d51c8831dadd7b87a4434', 'UHUI', 'Nathan', '01/01/1996', '3', 'emailetuLP17@example.com', 2, 2),
('34000018', 'fab005169e8332524cf98077755568e0', 'PAILLET', 'Yazhnarb', '03/01/1996', '3', 'emailetuLP18@example.com', 2, 2),
('34000019', 'ead6966e88948969925a291338dc1ca0', 'VARI', 'Neill', '05/01/1996', '3', 'emailetuLP19@example.com', 2, 2),
('34000020', '64d36ee61e2c8439a03b615ac450c0d5', 'BLANCHE', 'Paleulf', '07/01/1996', '3', 'emailetuLP20@example.com', 2, 2),
('34000021', 'd4ae83008f42a732f420392bda23062e', 'POMME', 'Nellwen', '09/01/1996', '3', 'emailetuLP21@example.com', 2, 2),
('34000022', '1d44192b78dca54030f4b22742906aa8', 'NEIGE', 'Aywynn', '11/01/1996', '3', 'emailetuLP22@example.com', 3, 2),
('34000023', '1d44192b78dca54030f4b22742906aa8', 'KILLER', 'Tobelambir', '15/01/1996', '3', 'emailetuLP23@example.com', 3, 2),
('34000024', 'a3fe0b6f880101e93c067c1b030644f5', 'FRITE', 'Raepinn', '19/01/1996', '3', 'emailetuLP24@example.com', 3, 2),
('34000025', 'ee5c1b447d77e4e1a17d53198b02991b', 'POIRE', 'Nathan', '21/01/1996', '3', 'emailetuLP25@example.com', 3, 2),
('34000026', '80df0db71f18b6f0405c9ac11e8b5ded', 'DEGOUT', 'Salress', '23/01/1996', '3', 'emailetuLP26@example.com', 3, 2),
('34000027', 'b2ae51a029f2770e6dacd95bbce39724', 'OREILLE', 'Vilehelm', '26/01/1996', '3', 'emailetuLP27@example.com', 3, 2),
('34000028', 'c82d2c387f8876764882ffa06826665d', 'PIED', 'Axel', '27/01/1996', '3', 'emailetuLP28@example.com', 3, 2),
('34000029', '7858e22f9ee90fe9c0081e5ce866e32d', 'ROMEO', 'Manon', '28/01/1996', '3', 'emailetuLP29@example.com', 3, 2),
('34000030', 'e4d139030f617f55eeb4000cb12360f5', 'ADRI', 'Lucas', '29/01/1996', '3', 'emailetuLP30@example.com', 3, 2),
('34000031', 'f290770b7ac0528b069a4ed9c320fc90', 'ADENA', 'Louna', '30/01/1996', '3', 'emailetuLP31@example.com', 3, 2),
('34000032', '161b196a0fc6dfb26dccacbc00294569', 'OUGUI', 'Léo', '30/01/1996', '3', 'emailetuLP32@example.com', 3, 2),
('35000017', '2b8470b1dd56a6b13404af3ac1238fa4', 'NOM17', 'PRENOM17', '01/01/1997', '2', 'emailetu17@example.com', 1, 1),
('35000018', '05efd2faf522216e9ae4ff7ec11dcc3b', 'NOM18', 'PRENOM18', '03/01/1997', '2', 'emailetu18@example.com', 1, 1),
('35000019', '0caaf7db5604eaef8c62400755f8a4fe', 'NOM19', 'PRENOM19', '05/01/1997', '2', 'emailetu19@example.com', 1, 1),
('35000020', '2fde8d040209d74cbe9746cc907b1a9e', 'NOM20', 'PRENOM20', '07/01/1997', '2', 'emailetu20@example.com', 1, 1),
('35000021', '33472c1e5f463feee0fd1ada60d1edd6', 'NOM21', 'PRENOM21', '09/01/1997', '2', 'emailetu21@example.com', 1, 1),
('35000022', '0c8ee966d20f8de3f1de92c4f66f044d', 'NOM22', 'PRENOM22', '11/01/1997', '2', 'emailetu22@example.com', 2, 1),
('35000023', 'f3cf71130ff78a8166b7dc9aed572840', 'NOM23', 'PRENOM23', '15/01/1997', '2', 'emailetu23@example.com', 2, 1),
('35000024', 'f36bb8df45473df1ade304abaec79db9', 'NOM24', 'PRENOM24', '19/01/1997', '2', 'emailetu24@example.com', 2, 2),
('35000025', 'c9b598ae95c54ad9d9eb4b9a9912c3cb', 'NOM25', 'PRENOM25', '21/01/1997', '2', 'emailetu25@example.com', 2, 2),
('35000026', '67611237b34cd0599ef3b462e8b45960', 'NOM26', 'PRENOM26', '23/01/1997', '2', 'emailetu26@example.com', 2, 2),
('35000027', '1c2ffd8712ebcc39da71e89264c8f33b', 'NOM27', 'PRENOM27', '26/01/1997', '2', 'emailetu27@example.com', 3, 2),
('35000028', '055c3515a87f5ed93cde1818aaea880b', 'NOM28', 'PRENOM28', '27/01/1997', '2', 'emailetu28@example.com', 3, 2),
('35000029', 'eb15b90be789acb1153d61c62115dd8e', 'NOM29', 'PRENOM29', '28/01/1997', '2', 'emailetu29@example.com', 3, 2),
('35000030', '30e5d606bdc7c5c08bbd6301c4194a11', 'NOM30', 'PRENOM30', '29/01/1997', '2', 'emailetu30@example.com', 3, 2),
('35000031', '84d10e9d3e3d8498cea9f6f0245a08a4', 'NOM31', 'PRENOM31', '30/01/1997', '2', 'emailetu31@example.com', 3, 2),
('35000032', '2e21243bba1ea7d549e892e7e9d933ae', 'NOM32', 'PRENOM32', '30/01/1997', '2', 'emailetu32@example.com', 3, 2),
('36000001', 'b02e7aae5192a3b32c5b8c52b75c5864', 'NOM1', 'PRENOM1', '01/01/1998', '1', 'emailetu1@example.com', 1, 1),
('36000002', '78da4a3016ca85f7be1043a59bc8f98f', 'NOM2', 'PRENOM2', '01/01/1998', '1', 'emailetu2@example.com', 1, 1),
('36000003', '858eb686f6846931a70ea1e4cfebcae4', 'NOM3', 'PRENOM3', '03/01/1998', '1', 'emailetu3@example.com', 1, 1),
('36000004', '07609a7c462e05621168f5ee8afb5cbe', 'NOM4', 'PRENOM4', '05/01/1998', '1', 'emailetu4@example.com', 1, 1),
('36000005', 'aa924743dc68d65dbd2ce162b5d07894', 'NOM5', 'PRENOM5', '07/01/1998', '1', 'emailetu5@example.com', 1, 1),
('36000006', '8c6ca7be0c21503285dbe7ef71a30876', 'NOM6', 'PRENOM6', '09/01/1998', '1', 'emailetu6@example.com', 2, 1),
('36000007', '62c7d9c8cdb1f7ca68f324ff9ebf5f68', 'NOM7', 'PRENOM7', '11/01/1998', '1', 'emailetu7@example.com', 2, 1),
('36000008', 'fb7faca339280d101567096930fe3c32', 'NOM8', 'PRENOM8', '15/01/1998', '1', 'emailetu8@example.com', 2, 2),
('36000009', '5542e3efe250ea6de523b97b44fbc79e', 'NOM9', 'PRENOM9', '19/01/1998', '1', 'emailetu9@example.com', 2, 2),
('36000010', 'ef41952ea988196fa79864e1b64d4acf', 'NOM10', 'PRENOM10', '21/01/1998', '1', 'emailetu10@example.com', 2, 2),
('36000011', '1894483d065664f3c58c3fad6a3fe235', 'NOM11', 'PRENOM11', '23/01/1998', '1', 'emailetu11@example.com', 2, 2),
('36000012', '18eee93e3efe5eb1d586aa5f911ba5de', 'NOM12', 'PRENOM12', '26/01/1998', '1', 'emailetu12@example.com', 3, 2),
('36000013', 'ba277e75e61158448811e141b258702e', 'NOM13', 'PRENOM13', '27/01/1998', '1', 'emailetu13@example.com', 3, 2),
('36000014', '6bb73cca40ea97832e832dbeeb81a11b', 'NOM14', 'PRENOM14', '28/01/1998', '1', 'emailetu14@example.com', 3, 2),
('36000015', '930f4156b527391296be54e9e76c9969', 'NOM15', 'PRENOM15', '29/01/1998', '1', 'emailetu15@example.com', 3, 2),
('36000016', '852934a20840e0745db5d8d77adcd91b', 'NOM16', 'PRENOM16', '30/01/1998', '1', 'emailetu16@example.com', 3, 2),
('rt', 'rt', 'rt', 'rt', 'rt', 'rt', 'rt', 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `MODULES`
--

CREATE TABLE `MODULES` (
  `num_m` varchar(25) NOT NULL,
  `nom_m` varchar(255) DEFAULT NULL,
  `ue_m` varchar(25) DEFAULT NULL,
  `semestre_m` varchar(25) DEFAULT NULL,
  `volumehoraire_s` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MODULES`
--

INSERT INTO `MODULES` (`num_m`, `nom_m`, `ue_m`, `semestre_m`, `volumehoraire_s`) VALUES
('M1101', 'Initiation aux réseaux dentreprises', '11', '1', 480),
('M1102', 'Initiation à la téléphonie dentreprise', '11', '1', 480),
('M1103', 'Architecture des équipements\r\ninformatiques', '11', '1', 480),
('M1104', 'Principes et architecture des réseaux', '11', '1', 480),
('M1105', 'Bases des systèmes dexploitation', '11', '1', 480),
('M1106', 'Initiation au développement Web', '11', '1', 480),
('M1108', 'Acquisition et codage de linformation', '11', '1', 480),
('M1109', 'PT : Mise en application de la\r\ncommunication et des techniques\r\ndocumentaires', '11', '1', 480),
('M1201', 'Anglais général de communication et initiation au vocabulaire technique', '12', '1', 480),
('M1202', 'EC: Éléments fondamentaux de la communication', '12', '1', 480),
('M1203', 'PPP: Connaître son champ dactivité', '12', '1', 480),
('M1204', 'Mise à niveau en numération et calculs', '12', '1', 480),
('M1205', 'Harmonisation des connaissances et des outils pour le signal', '12', '1', 480),
('M1206', 'Circuits électroniques : mise à niveau', '12', '1', 480),
('M1207', 'Bases de la programmation', '12', '1', 480),
('M1208', 'Adaptation et méthodologie pour la réussite Universitaire', '12', '1', 480),
('M2101', 'Réseaux locaux et équipements actifs', '21', '2', 540),
('M2102', 'Administration système', '21', '2', 540),
('M2103', 'Initiation aux réseaux dentreprises', '21', '2', 540),
('M2104', 'Bases de données', '21', '2', 540),
('M2105', 'Web dynamique', '21', '2', 540),
('M2106', 'Bases des services réseaux', '21', '2', 540),
('M2107', 'Principes des transmissions radio', '21', '2', 540),
('M2108', 'Chaine de transmission numérique', '21', '2', 540),
('M2109', 'PT : Description et planification de projet', '21', '2', 540),
('M2201', 'Développement de langlais technique et nouvelles technologies', '22', '2', 540),
('M2202', 'EC: Se documenter, informer et argumenter', '22', '2', 540),
('M2203', 'PPP: Formalisation du projet : mieux se connaître et préparer son stage', '22', '2', 540),
('M2204', 'Calcul différentiel et intégral', '22', '2', 540),
('M2205', 'Analyse de Fourier', '22', '2', 540),
('M2206', 'Bases de lélectromagnétisme pour la propagation', '22', '2', 540),
('M2207', 'Consolidation des bases de la programmation', '22', '2', 540),
('M2208', 'Consolidation de la méthodologie pour la réussite Universitaire', '22', '2', 540);

-- --------------------------------------------------------

--
-- Structure de la table `PROFESSEUR`
--

CREATE TABLE `PROFESSEUR` (
  `id_p` varchar(25) NOT NULL,
  `password_p` varchar(255) DEFAULT NULL,
  `nom_p` varchar(25) DEFAULT NULL,
  `prenom_p` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PROFESSEUR`
--

INSERT INTO `PROFESSEUR` (`id_p`, `password_p`, `nom_p`, `prenom_p`) VALUES
('2327805068', '735935299b7c563bd08a4546b7b9cfbd', 'NOMPROF9', 'PRENOMPROF9'),
('3337538564', '7231e5891fa8a8a8e1b3d6e34fa6c883', 'NOMPROF8', 'PRENOMPROF8'),
('5108583465', 'fd1762012deb8e1cc14d4d3abdf056c0', 'NOMPROF5', 'PRENOMPROF5'),
('5312887225', 'a9d137532024c7fd111d65274f0af835', 'NOMPROF10', 'PRENOMPROF10'),
('5976960327', 'f6fbed6f387e72eba9e79ac3319f421a', 'NOMPROF4', 'PRENOMPROF4'),
('6033737828', 'dcf1b1dfb3d09f1e798291ab374a2534', 'NOMPROF7', 'PRENOMPROF7'),
('6425393194', '6bee179a14bf3b8ec19545dae6cb9c0f', 'NOMPROF3', 'PRENOMPROF3'),
('6633630228', '5812e6bbdc2428aa59355f7020db2ced', 'NOMPROF6', 'PRENOMPROF6'),
('7236524406', '206c3a4fd74d69d2b4df95cb7022fc78', 'NOMPROF2', 'PRENOMPROF2'),
('9864782765', 'fd9b11833129d406a7140fe20b1b5467', 'NOMPROF1', 'PRENOMPROF1');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ABSENCE`
--
ALTER TABLE `ABSENCE`
  ADD PRIMARY KEY (`num_abs`),
  ADD KEY `id_e` (`id_e`),
  ADD KEY `num_m` (`num_m`),
  ADD KEY `num_m_2` (`num_m`),
  ADD KEY `num_m_3` (`num_m`),
  ADD KEY `num_m_4` (`num_m`);

--
-- Index pour la table `ETUDIANT`
--
ALTER TABLE `ETUDIANT`
  ADD PRIMARY KEY (`id_e`),
  ADD UNIQUE KEY `id_e` (`id_e`);

--
-- Index pour la table `MODULES`
--
ALTER TABLE `MODULES`
  ADD PRIMARY KEY (`num_m`),
  ADD UNIQUE KEY `num_m` (`num_m`),
  ADD KEY `num_m_2` (`num_m`),
  ADD KEY `num_m_3` (`num_m`),
  ADD KEY `num_m_4` (`num_m`),
  ADD KEY `num_m_5` (`num_m`),
  ADD KEY `num_m_6` (`num_m`);

--
-- Index pour la table `PROFESSEUR`
--
ALTER TABLE `PROFESSEUR`
  ADD PRIMARY KEY (`id_p`),
  ADD UNIQUE KEY `id_p` (`id_p`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `ABSENCE`
--
ALTER TABLE `ABSENCE`
  MODIFY `num_abs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ABSENCE`
--
ALTER TABLE `ABSENCE`
  ADD CONSTRAINT `absence_ibfk_1` FOREIGN KEY (`id_e`) REFERENCES `ETUDIANT` (`id_e`),
  ADD CONSTRAINT `absence_ibfk_2` FOREIGN KEY (`num_m`) REFERENCES `MODULES` (`num_m`);
