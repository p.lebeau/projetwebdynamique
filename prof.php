<?php
    session_start();
    require_once('connexionbdd.php');
    require_once('valider.php');   

?>

<!DOCTYPE html>
<html>
    
	 <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width" /> <!-- Afin de mettre en place un menu responsive, il faut obligatoirement ajouter cette balise à notre HTML permettant de communiquer à notre CSS la taille de l'écran de l'utilisateur -->
		<link rel="stylesheet" type="text/css" href="style.css" />
		<!-- <link rel="shortcut icon" type="image/x-icon" href="logoofficiel.png" /> --> <!-- ici on définit l'image en tant que logo s'affichant à gauche du titre dans l'onglet du navigateur -->
		<title> Projet Web dynamique </title>
 	</head>
    
    

    
<body>
    
    <div id=fenetre>
    <form method="POST" action="prof.php">
        
      <center>
          <b>
    <?php 
            $date = date("d-m-y");
            echo 'Nous sommes le '.$date;
            
    ?>
          </b>
        </center> 
        
        
    <p>     
        Année de la liste des étudiants à émarger :
        <input type="radio" name="annee" value="1" id="1" /> <label> RT1 </label>
        
        <input type="radio" name="annee" value="2" id="2"/> <label> RT2 </label>  
        
        <input type="radio" name="annee" value="3" id="3"/> <label> LP </label>

    </p>
        

         
    <p>     
        Pour quel groupe d'étudiant ?  
        <input type="radio" name="CM" value="4" id="4" /> <label> CM </label>
        
        <input type="radio" name="td" value="1" id="1" /> <label> TD1 </label>  
        
        <input type="radio" name="td" value="2" id="2" /> <label> TD2 </label> 
        
        <input type="radio" name="tp" value="1" id="1" /> <label> TP1 </label>
        
        <input type="radio" name="tp" value="2" id="2" /> <label> TP2 </label>  
        
        <input type="radio" name="tp" value="3" id="3" /> <label> TP3 </label> 
        

    </p>
        
     <p>
         <label> Module </label>
         <input type="text" placeholder="Par exemple : M2103" name="module" required="required" />
            </p>
        
        
     <p>
         <label> Durée de l'absence </label>
         <input type="text" placeholder="Par exemple : 2 (pour 2 heures)" name="dureeabs" required="required" />
    </p>
        
        
        <input name="envoyer" type="submit" class="button" value="Afficher"/>
        
    </form>
    

    <form method="POST" action="confirmefiche.php">
    
   <?php 
    
        $dureeabs=$_POST['dureeabs'];
        $module=$_POST['module'];
    ?>
    
    
<table>

    <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>ID</th>
        <th> Absent</th>
    </tr>
    
    <?php 
    
//REQUETE CONCERNANT LES CM
    $RequeteEtud = mysqli_query($con,"SELECT `id_e`, `nom_e`, `prenom_e` FROM ETUDIANT where annee_e = '".$_POST['annee']."' AND 4 = '".$_POST['CM']."'");
    
//REQUETE CONCERNANY LES TD ET TP
    $RequeteEtud1 = mysqli_query($con,"SELECT * FROM ETUDIANT where annee_e = '".$_POST['annee']."' AND (td_e = '".$_POST['td']."' OR tp_e='".$_POST['tp']."')" );
    
    
//DEBUT BOUCLE POUR AFFICHAGE DES ETUDIANTS SELON LES REQUETES CI-DESSUS
            while($donnee = mysqli_fetch_array($RequeteEtud1) or ($donnee = mysqli_fetch_array($RequeteEtud))) {
                $test=1;
            
                echo '<tr>
                        <th>'.$donnee['nom_e']. '</th>
                        <th>'.$donnee['prenom_e']. '</th>
                        <th>'.$donnee['id_e'].'</th>
                        <th>';?> <input type="checkbox" name="absence[]"  value='<?php  echo ($donnee['id_e']);?>'/> <?php echo '</th>
                    </tr>';     
            }
    ?>
    
</table>
    
    
 <?php 
 //CONDITION POUR AFFICHAGE BOUTON TERMINER   
        if($test==1)
            {
    
    ?>
    
<!-- PERMET D'ENVOYER LES VALEURS DES CHAMPS DE MANIERE CACHE-->
        <input name="Duree" type="hidden" value='<?php echo($dureeabs);?>'/> 
        <input name="num_m" type="hidden" value='<?php echo($module);?>'/> 
    
<!-- BOUTON TERMINER-->   
        <input name="envoyer" type="submit" class="button" value="Terminer"/>
            
</form>
        
 <?php 

        }
    
    ?>
                                                         
</div>
</body>
</html>
